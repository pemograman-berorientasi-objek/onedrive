# Use Case Onedrive
| No. | Use Case                                                                                          | Nilai |
|-----|---------------------------------------------------------------------------------------------------|-------|
| 1.  | Mengunggah file dari perangkat lokal ke OneDrive.                                                 | 10     |
| 2.  | Melkukan registerUser                                                                             | 10     |
| 3.  | Membuat Folder                                                                                    | 10     |
| 4.  | Menampilkan dan mengatur folder dan file di OneDrive.                                             | 9     |
| 5.  | Mengedit file langsung di OneDrive.                                                               | 8     |
| 6.  | Mengunduh file dari OneDrive ke perangkat lokal.                                                  | 9     |
| 7.  | Menghapus file yang tidak diperlukan dari OneDrive.                                               | 8     |
| 8.  | Membuat folder baru di OneDrive.                                                                  | 7     |
| 9.  | Mengganti nama file dan folder di OneDrive.                                                       | 7     |
| 10. | Mencari file dan folder di OneDrive.                                                              | 8     |
| 11. | Membuat salinan file di OneDrive.                                                                 | 8     |
| 12. | Membuka dan melihat file di OneDrive.                                                             | 8     |
| 13. | Mengatur hak akses dan izin berbagi untuk file dan folder di OneDrive.                            | 9     |
| 14. | Mengatur file dan folder dengan kategori dan tag di OneDrive.                                     | 7     |
| 15. | Mengelola versi file di OneDrive.                                                                 | 8     |
| 16. | Membatalkan tindakan atau pemulihan file yang dihapus di OneDrive.                                | 8     |
| 17. | Mengatur pemberitahuan dan notifikasi untuk aktivitas di OneDrive.                                | 7     |
| 18. | Mengarsipkan file dan folder di OneDrive.                                                         | 7     |
| 19. | Mengatur pemulihan file yang hilang atau terhapus di OneDrive.                                    | 8     |
| 20. | Mengedit metadata file dan folder di OneDrive.                                                    | 7     |
| 21. | Menandai file dan folder favorit di OneDrive untuk akses cepat.                                   | 6     |
| 22. | Mengatur dan mengelola album foto di OneDrive.                                                    | 8     |
| 23. | Mengelola file audio dan video di OneDrive.                                                       | 7     |
| 24. | Mengelola dan mengedit dokumen di OneDrive.                                                       | 9     |
| 25. | Mengelola file PDF di OneDrive.                                                                   | 8     |
| 26. | Mengorganisir file dan folder dengan fitur tata letak di OneDrive.                                | 7     |
| 27. | Mengelola file yang dibagikan oleh pengguna lain di OneDrive.                                     | 8     |
| 28. | Mengamankan file dengan enkripsi di OneDrive.                                                     | 9     |
| 29. | Menggunakan OneDrive sebagai cadangan dan penyimpanan file.                                       | 9     |
| 30. | Mengelola perangkat yang terhubung dengan OneDrive.                                              | 7     |
| 31. | Mengatur jadwal sinkronisasi OneDrive dengan perangkat lokal.                                     | 8     |
| 32. | Mengelola batasan ruang penyimpanan di OneDrive.                                                  | 7     |
| 33. | Menggabungkan dan membagikan folder di OneDrive.                                                  | 8     |
| 34. | Mengimpor file dari layanan cloud lain ke OneDrive.                                               | 8     |
| 35. | Mengintegrasikan OneDrive dengan aplikasi pihak ketiga.                                           | 8     |
| 36. | Melihat riwayat aktivitas dan log di OneDrive.                                                    | 7     |
| 37. | Mengelola akses jarak jauh ke OneDrive.                                                           | 8     |
| 38. | Menggunakan OneDrive di perangkat mobile.                                                         | 9     |
| 39. | Mengatur tautan berbagi yang memiliki tanggal kedaluwarsa di OneDrive.                            | 7     |
| 40. | Mengatur folder dan file dengan pemindahan batch di OneDrive.                                     | 8     |

# Soal Interview
NO | Pertanyaan | Jawaban
---|---|---
1 | Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait) | [Jawaban 1](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#1-mampu-mendemonstrasikan-penyelesaian-masalah-dengan-pendekatan-matematika-dan-algoritma-pemrograman-secara-tepat-lampirkan-link-source-code-terkait)
2 | Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait) | [Jawaban 2](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#2-mampu-menjelaskan-algoritma-dari-solusi-yang-dibuat-lampirkan-link-source-code-terkait)
3 | Mampu menjelaskan konsep dasar OOP | [Jawaban 3](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#3-mampu-menjelaskan-konsep-dasar-oop)
4 | Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait) | [Jawaban 4](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#4-mampu-mendemonstrasikan-penggunaan-encapsulation-secara-tepat-lampirkan-link-source-code-terkait)
5 | Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait) | [Jawaban 5](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#5-mampu-mendemonstrasikan-penggunaan-abstraction-secara-tepat-lampirkan-link-source-code-terkait)
6 | Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait) | [Jawaban 6](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#6-mampu-mendemonstrasikan-penggunaan-inheritance-dan-polymorphism-secara-tepat-lampirkan-link-source-code-terkait)
7 | Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ? | [Jawaban 7](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#7-mampu-menerjemahkan-proses-bisnis-ke-dalam-skema-oop-secara-tepat-bagaimana-cara-kamu-mendeskripsikan-proses-bisnis-kumpulan-use-case-ke-dalam-oop-)
8 | Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait) | [Jawaban 8](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#8-mampu-menjelaskan-rancangan-dalam-bentuk-class-diagram-dan-use-case-table-lampirkan-diagram-terkait)
9 | Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait) | [Jawaban 9](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#9-mampu-memberikan-gambaran-umum-aplikasi-kepada-publik-menggunakan-presentasi-berbasis-video-lampirkan-link-youtube-terkait)
10 | Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github) | [Jawaban 10](https://gitlab.com/asrulmfadhlan/jawaban-interview/-/blob/main/jawaban-interview.md#10-inovasi-ux-lampirkan-url-screenshot-aplikasi-di-gitlab-github)

